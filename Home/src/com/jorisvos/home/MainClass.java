// Created by jorisvos
package com.jorisvos.home;

import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;

public final class MainClass extends JavaPlugin {
	ConsoleCommandSender console = Bukkit.getConsoleSender();
	
	public void onEnable() { console.sendMessage(ChatColor.GOLD + "[Home] " + ChatColor.GREEN + "Homes version V"+getDescription().getVersion()+" is enabled!"); }
	public void onDisable() { console.sendMessage(ChatColor.GOLD + "[Home] " + ChatColor.RED + "Homes version V"+getDescription().getVersion()+" is disabled!"); }
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if (cmd.getName().equalsIgnoreCase("home")) 
		{
			if (!(sender instanceof Player))
				sender.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.RED + "You have to be a player to execute this command!");
			else
			{
				Player player = (Player)sender;
				
				if (args.length == 0)
				{
					ConfigurationSection configSection = getConfig().getConfigurationSection("users."+player.getUniqueId());
					
					if (configSection == null)
						player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.BLUE + "You have no homes set. Use '/sethome'");
					else
					{					
						Set<String> homes = configSection.getKeys(false);
						if (homes.size() > 1)
						{
							player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.BLUE + "You have more than 1 home, use the command like this '/home <homeName>'");
							
							String stringHomes = "";
							for (String home : homes)
								stringHomes += home + ", ";
							stringHomes = stringHomes.substring(0, stringHomes.length() - 2);
							
							player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.BLUE + "Homes: "+stringHomes);
						}
						else
						{
							if (isSetConfigString(player.getUniqueId()))
							{						
								player.teleport(getLocationFromConfig(player.getUniqueId()));
								player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.GREEN + "Home!");
							}
							else
								player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.BLUE + "You need to set your home. Use '/sethome'");
						}
					}
				}
				else if (args.length == 1)
				{
					if (isSetConfigString(player.getUniqueId(), args[0]))
					{
						player.teleport(getLocationFromConfig(player.getUniqueId(), args[0]));
						player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.GREEN + "Home!");
					}
					else
						player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.BLUE + "You need to set that home. The command is '/sethome <homeName>'");
				}
				else if (args.length == 2)
				{
					if (player.hasPermission("home.home.other"))
					{
						@SuppressWarnings("deprecation")
						OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);
						
						if (isSetConfigString(offlinePlayer.getUniqueId(), args[1]))
						{
							player.teleport(getLocationFromConfig(offlinePlayer.getUniqueId(), args[1]));
							player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.GREEN + "Home!");
						}
						else
							player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.BLUE + "That home of that player does not exists.");
					}
					else
						player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.RED + "You don't have that permission");
				}
				else
					player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.BLUE + "Too many arguments.");
			}
			
			return true;
		}
		else if (cmd.getName().equalsIgnoreCase("sethome"))
		{
			if (!(sender instanceof Player))
				sender.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.RED + "You have to be a player to execute this command!");
			else
			{
				Player player = (Player)sender;
				
				Location loc = player.getLocation();
				if (args.length == 0)
				{
					if (saveLocationToConfig(player.getUniqueId(), loc))
						player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.GREEN + "Home set!");
					else
						player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.BLUE + "Home name already used!");
				}
				else if (args.length == 1 && player.hasPermission("home.sethome.multiple"))
				{
					if (saveLocationToConfig(player.getUniqueId(), loc, args[0]))
						player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.GREEN + "Home set!");
					else
						player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.BLUE + "Home name already used!");
				}
				else if (args.length == 1 && !player.hasPermission("home.sethome.multiple"))
					player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.RED + "You don't have that permission");
				else if (args.length == 2 && player.hasPermission("home.sethome.other"))
				{
					@SuppressWarnings("deprecation")
					OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);
					
					if (saveLocationToConfig(offlinePlayer.getUniqueId(), loc, args[1]))
						player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.GREEN + "Home set!");
					else
						player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.BLUE + "Home name already used!");
				}
				else if (args.length == 2 && !player.hasPermission("home.sethome.other"))
					player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.RED + "You don't have that permission");
				else
					player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.BLUE + "Too many arguments.");
			}
			
			return true;
		}
		else if (cmd.getName().equalsIgnoreCase("delhome"))
		{
			Player player = null;
			
			if (args.length == 0)
			{
				player = (Player)sender;
				
				if (removeLocationToConfig(player.getUniqueId()))
					player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.GREEN + "Home removed!");
				else
					player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.BLUE + "Home does not exists!");
			}
			else if (args.length == 1)
			{
				player = (Player)sender;
				
				if (removeLocationToConfig(player.getUniqueId(), args[0]))
					player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.GREEN + "Home removed!");
				else
					player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.BLUE + "Home does not exists!");
			}
			else if (args.length == 2)
			{
				@SuppressWarnings("deprecation")
				OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);
				
				if (!sender.hasPermission("home.delhome.other"))
					sender.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.RED + "You don't have that permission");
				else
				{
					if (removeLocationToConfig(offlinePlayer.getUniqueId(), args[1]))
						sender.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.GREEN + "Home removed!");
					else
						sender.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.BLUE + "Home or player does not exists!");
				}
			}
			else
				sender.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[Home] " + ChatColor.BLUE + "Too many arguments.");
			
			return true;
		}
		
		return false;
	}
	
	public Location getLocationFromConfig(UUID playerName) { return getLocationFromConfig(playerName, "home"); }
	public Location getLocationFromConfig(UUID playerName, String homeName)
	{
		World world = getServer().getWorld(getConfig().getString("users."+playerName+"."+homeName+".world"));
		
		Double x = getConfig().getDouble("users."+playerName+"."+homeName+".x");
		Double y = getConfig().getDouble("users."+playerName+"."+homeName+".y");
		Double z = getConfig().getDouble("users."+playerName+"."+homeName+".z");
		
		Integer yaw = getConfig().getInt("users."+playerName+"."+homeName+".yaw");
		Integer pitch = getConfig().getInt("users."+playerName+"."+homeName+".pitch");

		return new Location(world, x, y, z, yaw, pitch);
	}
	
	public Boolean saveLocationToConfig(UUID playerName, Location homeLocation) { return saveLocationToConfig(playerName, homeLocation, "home"); }
	public Boolean saveLocationToConfig(UUID playerName, Location homeLocation, String homeName)
	{
		//if (getConfig().isSet("users."+playerName+"."+homeName))
		//	return false;
		//else
		//{
			getConfig().set("users."+playerName+"."+homeName+".world", homeLocation.getWorld().getName());
			
			getConfig().set("users."+playerName+"."+homeName+".x", Double.valueOf(homeLocation.getX()));
			getConfig().set("users."+playerName+"."+homeName+".y", Double.valueOf(homeLocation.getY()));
			getConfig().set("users."+playerName+"."+homeName+".z", Double.valueOf(homeLocation.getZ()));
			
			getConfig().set("users."+playerName+"."+homeName+".yaw", Double.valueOf(homeLocation.getYaw()));
			getConfig().set("users."+playerName+"."+homeName+".pitch", Double.valueOf(homeLocation.getPitch()));
			
			saveConfig();			
			return true;
		//}
	}
	
	public Boolean removeLocationToConfig(UUID playerName) { return removeLocationToConfig(playerName, "home"); }
	public Boolean removeLocationToConfig(UUID playerName, String homeName)
	{
		if (!getConfig().isSet("users."+playerName+"."+homeName))
			return false;
		else
		{
			getConfig().set("users."+playerName+"."+homeName, null);
			
			saveConfig();
			return true;
		}
	}
	
	public Boolean isSetConfigString(UUID playerName) { return isSetConfigString(playerName, "home"); }
	public Boolean isSetConfigString(UUID playerName, String homeName) { return getConfig().isSet("users."+playerName+"."+homeName); }
}
