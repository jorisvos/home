# Home

A minecraft home plugin

# Usage

* Download the plugin
* Drag the downloaded file (Home.jar) to your plugin folder
* Reload the server (or stop the server and start it again)
* The server automatically creates a config.yml file and you're ready to go

# Commands

## Default commands

* /home
* /sethome
* /delhome

## OP commands

#####*[player] and [name] are both optional (if you want to use [player] you will have to use [name]*
* /home [player] [name]
* /sethome [player] [name]
* /delhome [player] [name]

# Permissions

* home.*				-	Gives you all the permissions
* home.home 			-	Gives you access to the /home [name] command
* home.sethome			- 	Gives you access to the /sethome command
* home.delhome			-	Gives you access to the /delhome [name] command
* home.home.other		-	Gives you access to the /home [player] [name] command
* home.sethome.other	- 	Gives you access to the /sethome [player] [name] command
* home.delhome.other	-	Gives you access to the /delhome [player] [name] command
* home.sethome.multiple	- 	Gives you access to the /sethome [name] command
* home.denied			-	Prevents the user from using the Home plugin